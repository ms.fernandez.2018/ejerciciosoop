
class Cuenta:

    def __init__(self,numero_cuenta, saldo, titular):
        self.numero_cuenta=numero_cuenta
        self.saldo=saldo
        self.titular=titular

    def ingreso(self,ingreso):
        self.saldo=self.saldo+ingreso
        print("Se realiza ingreso")
        return

    def reintegro(self,reintegro):
        self.saldo=self.saldo-reintegro
        print("Se realiza reintegro")
        return

    def transferencia(self,transferencia,destino):
        print("Se realiza trasnferencia a {destino}".format(destino=destino))
        self.reintegro(transferencia)
        return

    def __str__(self):
        return "Hola {titular}, tras todos tus movimientos en tu cuenta {numero_cuenta} tienes un saldo de {saldo} euros".format(
    titular=self.titular, numero_cuenta=self.numero_cuenta, saldo=self.saldo)

titularJuan=Cuenta(12739813,3000,"Juan")
print(titularJuan)
titularJuan.ingreso(2000)
print(titularJuan)
titularJuan.reintegro(1000)
titularJuan.transferencia(3000,723493445)
print(titularJuan)

